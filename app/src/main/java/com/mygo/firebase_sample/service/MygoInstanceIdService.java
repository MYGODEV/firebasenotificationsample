package com.mygo.firebase_sample.service;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.xy.utils.Logger;

/**
 * Created by wuyiru on 2017/4/26.
 */

public class MygoInstanceIdService extends FirebaseInstanceIdService {

    private static final Logger LOGGER = Logger.getInstance(MygoInstanceIdService.class);

    @Override
    public void onTokenRefresh() {
        String currentToken = FirebaseInstanceId.getInstance().getToken();

        LOGGER.d("FCM currentToken: " + currentToken);
    }
}
