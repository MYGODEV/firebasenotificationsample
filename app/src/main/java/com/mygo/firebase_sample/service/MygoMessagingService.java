package com.mygo.firebase_sample.service;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.NotificationCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.mygo.firebase_sample.R;
import com.mygo.firebase_sample.activity.MainActivity;
import com.mygo.firebase_sample.data.KeyData;
import com.mygo.firebase_sample.data.MessageDataObj;
import com.mygo.firebase_sample.data.database.DatabaseUtil;
import com.mygo.firebase_sample.manager.NotificationsManager;
import com.xy.utils.Logger;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by wuyiru on 2017/4/26.
 */

public class MygoMessagingService extends FirebaseMessagingService {
    private Logger LOGGER = Logger.getInstance(this.getClass());

    private ArrayList<MessageDataObj> messageDataObjArrayList = null;
    private DatabaseUtil databaseUtil;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        databaseUtil = new DatabaseUtil(getBaseContext());

        messageDataObjArrayList = new ArrayList<>();
        messageDataObjArrayList.clear();

        Map<String, String> remoteMessageData = remoteMessage.getData();
        LOGGER.d("FCM RemoteMessage.data: " + remoteMessageData.toString());
        String time = remoteMessageData.get(KeyData.TIME);
        String map = remoteMessageData.get(KeyData.MAP);
        String image = remoteMessageData.get(KeyData.IMAGE);
        String video = remoteMessageData.get(KeyData.VIDEO);
        String sound = remoteMessageData.get(KeyData.SOUND);

        final String title = remoteMessage.getNotification().getTitle();
        final String message = remoteMessage.getNotification().getBody();
        final String largeImageUrl = remoteMessage.getData().get(KeyData.LARGE_IMAGE);

        saveRemoteMessageData(time, map, image, video, sound, title, message);

        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                Glide.with(getBaseContext())
                        .load(largeImageUrl)
                        .asBitmap()
                        .into(new SimpleTarget<Bitmap>() {
                            @Override
                            public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                                LOGGER.d("FCM Received, and now Loaded Image!");
                                NotificationsManager.getInstance().generateNotification(getBaseContext(), resource, title, message);
                            }

                            @Override
                            public void onLoadFailed(Exception e, Drawable errorDrawable) {
                                super.onLoadFailed(e, errorDrawable);
                                NotificationsManager.getInstance().generateNotification(getBaseContext(), title, message);
                            }
                        });
            }
        });
    }

    private void saveRemoteMessageData(String title, String message, String time,
                                       String map, String image, String video, String sound) {
        MessageDataObj messageDataObj = new MessageDataObj();
        messageDataObj.setTitle(title);
        messageDataObj.setMessage(message);
        messageDataObj.setTime(time);
        messageDataObj.setMap(map);
        messageDataObj.setImage(image);
        messageDataObj.setVideo(video);
        messageDataObj.setSound(sound);
        messageDataObjArrayList.add(messageDataObj);
        databaseUtil.saveMessageDataArrayList(messageDataObjArrayList);
    }

    private void sendNotification(String title, String messageBody) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        long[] vibrate_effect =
                {1000, 500, 1000, 400, 1000, 300, 1000, 200, 1000, 100};

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setVibrate(vibrate_effect)
                .setContentTitle(title)
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }
}
