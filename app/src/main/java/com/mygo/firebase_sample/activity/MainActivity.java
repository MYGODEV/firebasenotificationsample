package com.mygo.firebase_sample.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.mygo.firebase_sample.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
