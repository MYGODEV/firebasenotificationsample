package com.mygo.firebase_sample.data;

/**
 * Created by wuyiru on 2017/4/26.
 */

public class KeyData {
    public static final String LARGE_IMAGE = "large_image";
    public static final String MAP   = "map";
    public static final String IMAGE = "image";
    public static final String VIDEO = "video";
    public static final String SOUND = "sound";
    public static final String TIME  = "time";

}
