package com.mygo.firebase_sample.data.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

import com.mygo.firebase_sample.data.MessageDataObj;

import java.util.ArrayList;

/**
 * Created by wuyiru on 2017/4/27.
 */

public class DatabaseUtil extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "MyGo.db";
    private static final int DATABASE_VERSION = 1;
    private static final String TABLE_NAME_SETTING = "Setting";
    private static final String TABLE_NAME_MESSAGE_DATA = "MessageData";

    private SQLiteDatabase db;

    public DatabaseUtil(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        db = this.getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        CreateTable(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        DeleteTable(db);
        CreateTable(db);
    }

    private void CreateTable(SQLiteDatabase db){
        db.execSQL("CREATE TABLE " + TABLE_NAME_SETTING
                + " (_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                + " _key TEXT,"
                + " _value TEXT);"
        );

        db.execSQL("CREATE TABLE " + TABLE_NAME_MESSAGE_DATA
                + " (_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                + " _title TEXT,"
                + " _message TEXT,"
                + " _time TEXT,"
                + " _map TEXT,"
                + " _image TEXT,"
                + " _video TEXT,"
                + " _sound TEXT);"
        );
    }

    private void DeleteTable(SQLiteDatabase db){
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_SETTING);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_MESSAGE_DATA);
    }

    /**
     * 取得設定資料
     * @param setting 設定名稱
     * @return String
     */
    public String getSetting(String setting){
        try{
            Cursor cursor = db.query(TABLE_NAME_SETTING, null, null, null, null, null, null);
            if (cursor.moveToFirst()){
                do {
                    if (setting.equals(cursor.getString(1)))
                        return cursor.getString(2);
                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 刪除設定資料
     * @param setting 設定名稱
     */
    public void DeleteSetting(String setting){
        long row = 0;
        try {
            row = db.delete(TABLE_NAME_SETTING, "_key = '" + setting + "'", null);
        } catch (SQLiteException e){
            e.printStackTrace();
        }
    }


    /**
     * 更新設定資料
     * @param setting 設定名稱
     * @param value 設定值
     */
    public void UpdateSetting(String setting, String value){
        ContentValues cv = new ContentValues();
        cv.put("_key", setting);
        cv.put("_value", value);
        long row = 0;
        try {
            row = db.update(TABLE_NAME_SETTING, cv, "_key = '" + setting + "'", null);
        } catch (SQLiteException e){
            e.printStackTrace();
        }
        if (row < 1) row = db.insert(TABLE_NAME_SETTING, null, cv);
        cv.clear();
    }

    public void UpdateSetting(String setting, int value){
        UpdateSetting(setting, String.valueOf(value));
    }

    public void UpdateSetting(String setting, float value){
        UpdateSetting(setting, String.valueOf(value));
    }

    public void UpdateSetting(String setting, double value){
        UpdateSetting(setting, String.valueOf(value));
    }

    public void UpdateSetting(String setting, long value){
        UpdateSetting(setting, String.valueOf(value));
    }

    public void UpdateSetting(String setting, boolean value){
        UpdateSetting(setting, String.valueOf(value ? "1" : "0"));
    }

//    /**
//     * 搜尋歷史紀錄
//     * @author wuyiru
//     *
//     */
//    public static class SearchHistory{
//        int Order;
//        String key;
//        String jsonObject;
//    }

    public ArrayList<MessageDataObj> getMessageDataArrayList(){
        ArrayList<MessageDataObj> messageDataArrayList = new ArrayList<>();
        try {
            Cursor cursor = db.query(TABLE_NAME_MESSAGE_DATA, null, null, null, null, null, null);
            if (cursor.moveToFirst()){
                do {
                    String message = cursor.getString(cursor.getColumnIndex("_message"));
                    String time    = cursor.getString(cursor.getColumnIndex("time"));
                    String map = cursor.getString(cursor.getColumnIndex("_map"));
                    String image = cursor.getString(cursor.getColumnIndex("_image"));
                    String video = cursor.getString(cursor.getColumnIndex("_video"));
                    String sound = cursor.getString(cursor.getColumnIndex("_sound"));

                    MessageDataObj messageDataObj = new MessageDataObj();
                    messageDataObj.setMessage(message);
                    messageDataObj.setTime(time);
                    messageDataObj.setMap(map);
                    messageDataObj.setImage(image);
                    messageDataObj.setVideo(video);
                    messageDataObj.setSound(sound);
                    messageDataArrayList.add(messageDataObj);
                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return messageDataArrayList;
    }

    public void saveMessageDataArrayList(ArrayList<MessageDataObj> messageDataArrayList){
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_MESSAGE_DATA);

        db.execSQL("CREATE TABLE " + TABLE_NAME_MESSAGE_DATA
                        + " (_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                        + " _title TEXT,"
                        + " _message TEXT,"
                        + " _time TEXT,"
                        + " _map TEXT,"
                        + " _image TEXT,"
                        + " _video TEXT,"
                        + " _sound TEXT);"
        );
        db.beginTransaction();

        for (int i = 0; i < messageDataArrayList.size(); i++){
            ContentValues contentValues = new ContentValues();
            contentValues.put("_title", messageDataArrayList.get(i).getTitle());
            contentValues.put("_message", messageDataArrayList.get(i).getMessage());
            contentValues.put("_time", messageDataArrayList.get(i).getTime());
            contentValues.put("_map", messageDataArrayList.get(i).getMap());
            contentValues.put("_image", messageDataArrayList.get(i).getImage());
            contentValues.put("_video", messageDataArrayList.get(i).getVideo());
            contentValues.put("_sound", messageDataArrayList.get(i).getSound());
            long row = db.insert(TABLE_NAME_MESSAGE_DATA, null, contentValues);
            contentValues.clear();
        }
        db.setTransactionSuccessful();
        db.endTransaction();
    }

    public void deleteMessageDataByIndexName(String indexName) {
        String[] whereValue ={ indexName };
        try {
            db.delete(TABLE_NAME_MESSAGE_DATA, "_title = ?", whereValue);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteAllMessageData() {
        try {
            db.delete(TABLE_NAME_MESSAGE_DATA, null, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
